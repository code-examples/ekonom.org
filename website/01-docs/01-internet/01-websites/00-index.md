---
layout: page
title: Сайтостоение
permalink: /internet/websites/
---

# Сайтостоение

### [Экономь на регистрации и продлении доменов](/internet/websites/prodlenie-domenov/)

### [Бесплатные SSL сертификаты для сайтов](/internet/websites/ssl-for-free/)

### [Бесплатный мониторинг доступности сайта](/internet/websites/website-monitor/)

### Бесплтаный хостинг статических сайтов.

Т.е. сайтов, которым не нужны базы данных, серверные вычисления (php, java, node, ruby etc.) - github.com. Смотрите технологию gh-pages. Свой домен подключить бесплатно можно! Например, itsimple.ru сейчас работает именно там.


### Бесплтаные облака

Oracle дает бесплатно 2 виртуальные машины для любых целей + 2 виртуальные машины под базы https://www.oracle.com/cloud/free/

Вроде как навсегда. Да не самые мощные, но зато бесплатно.