---
layout: page
title: Бесплатный мониторинг доступности сайта
permalink: /internet/websites/website-monitor/
---

# Бесплатный мониторинг доступности сайта

https://uptimerobot.com/

50 Monitors, Checked Every 5 Minutes, Totally Free!

![Бесплатный мониторинг доступности сайта](/img/uptimerobot.png "Бесплатный мониторинг доступности сайта"){: .center-image }
